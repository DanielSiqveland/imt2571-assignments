<?php
include_once("IModel.php");
include_once("Book.php");
//include_once("TestDBProps.php");

$servername = "localhost";
$username = "username";
$passwod = "password";
$dbname = "test";

/** The Model is the class holding data about a collection of books.
 * @author Rune Hjelsvold
 * @see http://php-html.net/tutorials/model-view-controller-in-php/ The tutorial code used as basis.
 */
class DBModel implements IModel
{

    protected $db = null;

    /**
	 * @throws PDOException
     */
    public function __construct($db = null)
    {
	    if ($db)
		{
			$this->db = $db;
		}
		else  // Create PDO connection
		{
    try {
          $this->db = new PDO('mysql:host=localhost; dbname=test; charset=utf8', 'root', '');
            // set the PDO error mode to exception
          $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
         }
      catch(PDOException $e) {
            echo "Oppkobling til host failet: " . $e->getMessage();
          }
		}
    }


    public function getBookList()
    {
		    $booklist = array();
        try{
        $sth = $this->db->query("SELECT * FROM book ORDER BY id");
        $booklist= $sth->fetchAll(PDO::FETCH_OBJ); //ASSOC?  (booklist vs $row)
          /*foreach ($rows as $row) {
            $booklist[$row['id']] = new Book ($row['title'],$row['author'],$row['description'],$row['id']);
          // */
        return $booklist;
      }
      catch(PDOException $e){
        $e->getMessage();
      }
    }

    public function getBookById($id)
    {
      if(!is_numeric($id)){throw new Exception("ASDFGASFD");}
		    $book = null;
        try{
        $row = $this->db->query("SELECT * FROM book WHERE id=$id")->fetch(PDO::FETCH_ASSOC);
        if($row){
        $book = new Book($row['title'], $row['author'], $row['description'], $row['id']);
          return $book;
        } else return NULL;
      }
      catch(PDOException $e){
        $e->getMessage();
      }
    }

	/**{
		function getData($db) {
		$stmt = $db->query("SELECT * FROM table");
		return $stmt->fetchAll(PDO::FETCH_ASSOC);
}

	try {
	getData($db);
	} catch(PDOException $ex) {
	//handle me.
	*echo "GetBookById funka ikke";
	*}
	*
	*	$book = null;
  *      return $book;
    }
    
*/
    public function addBook($book)
    {
      if(($book->title && $book->author)) {
        try{
            $stmt = $this->db->prepare("INSERT INTO book(title, author, description) VALUES (:title, :author, :description)");
            $stmt->bindValue(':title', $book->title);
            $stmt->bindValue(':author', $book->author);
            $stmt->bindValue(':description', $book->description);
            $stmt->execute();
            $book->id = $this->db->lastInsertId();
           }
          catch(PDOException $e){
            $e->getMessage();
          }
      } else throw new PDOException("Tittel eller forfatter ble ikke fylt ut!");
    }

    /** Modifies data related to a book in the collection.
     * @param $book Book The book data to be kept.
     * @todo Implement function using PDO and a real database.
     */
    public function modifyBook($book)
    {
      if(($book->title && $book->author)) {
        try{
            $stmt = $this->db->prepare('UPDATE book SET title=:title, author=:author, description=:description WHERE id=' .
            $book->id);
            $stmt->bindValue(':title', $book->title, PDO::PARAM_STR);
            $stmt->bindValue(':author',$book->author, PDO::PARAM_STR);
            $stmt->bindValue(':description',$book->description, PDO::PARAM_STR);
            $stmt->execute();
          }
          catch(PDOException $e){
            $e->getMessage();
          }
        }
    }

    /** Deletes data related to a book from the collection.
     * @param $id integer The id of the book that should be removed from the collection.
     */
    public function deleteBook($id)
    {
        $delete = $this->db->prepare("DELETE FROM book WHERE id=$id");
        //$delete->bindValue(':id',$id);
        $delete->execute();
    }
	
}

?>